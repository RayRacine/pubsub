#lang info
(define collection "pubsub")
(define deps '("typed-racket-more" "typed-racket-lib" "base" ))
(define build-deps '("scribble-lib" "typed-racket-doc" "racket-doc" "rackunit-lib"))
;;(define scribblings '(("scribblings/csrmesh.scrbl" ())))
(define pkg-desc "Publish Subscrib 1-N in Typed Racket.")
(define version "0.1.0")
(define pkg-authors '("Raymond Racine"))

